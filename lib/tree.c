
/**
 * @file tree.c
 * @author Matsievskiy S.V.
 * @brief Tree related functions.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <tree.h>
#include <string.h>

#if defined(DESKTOPBUILD)
#else
#include <mem.h>

#define sdm_malloc(x) os_zalloc(x) /**< Allocate memory. */

#define sdm_free(x) os_free(x) /**< Free memory. */
#endif

struct device *tree_init(void)
{
        if (sdm)
                goto allocated;

        sdm = sdm_malloc(sizeof(struct sdm));
        if (!sdm)
                goto sdm_alloc_err;
        sdm->magic = SDM_MAGIC;
        SDM_DEBUG_MSG("sdm allocated");

        struct str *root_name = push_str("ESP8266", 7);

        if (!root_name)
                goto name_alloc_err;

        struct device root = {.magic = DEV_MAGIC,
                .enabled = 0,
                .refcount = 0,
                .name = root_name
        };

        struct device *root_heap =
            llist_push(&sdm->devices, &root, sizeof(root));

        if (!root_heap)
                goto bus_alloc_err;

        sdm->root = root_heap;

        SDM_DEBUG_MSG("root bus added");

        return root_heap;

 bus_alloc_err:
        llist_delete(&sdm->strings, 0);
 name_alloc_err:
        sdm_free(sdm);
 sdm_alloc_err:
 allocated:
        SDM_DEBUG_MSG("sdm init failed");
        return NULL;
}

void tree_destroy(void)
{
        if (chk_sdm(sdm)) {
                SDM_DEBUG_MSG("freeing sdm tree");
                if (&sdm->devices) {
                        llist_map(&sdm->devices, __devices_free_nodes, NULL, 0);
                        llist_free(&sdm->devices);
                }
                if (&sdm->drivers) {
                        llist_map(&sdm->drivers, __drivers_free_nodes, NULL, 0);
                        llist_free(&sdm->drivers);
                }
                if (&sdm->strings) {
                        llist_free(&sdm->strings);
                }
                sdm_free(sdm);
                sdm = NULL;     // dereference pointer. If not done, future init will fail
        } else {
                SDM_DEBUG_MSG("sdm not exist");
        }
}

struct device *sdm_root(void)
{
        if (!chk_sdm(sdm) || !chk_dev(sdm->root))
                goto err;
        return sdm->root;

 err:
        return NULL;
}

struct driver *driver_next(size_t *iter)
{
        if (*iter == LLIST_ERR)
                goto stop_condition;
        struct driver *drv = llist_index(&(sdm->drivers), *iter);

        if (chk_drv(drv)) {
                (*iter)++;
                return drv;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

struct device *device_next(size_t *iter)
{
        if (*iter == LLIST_ERR)
                goto stop_condition;
        struct device *dev = llist_index(&(sdm->devices), *iter);

        if (chk_dev(dev)) {
                (*iter)++;
                return dev;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

struct method *driver_method_next(const struct driver *drv, size_t *iter)
{
        if ((!chk_drv(drv)) || (*iter == LLIST_ERR))
                goto stop_condition;

        struct method *met = llist_index(&(drv->methods), *iter);

        if (chk_met(met)) {
                (*iter)++;
                return met;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

struct attr *driver_attr_next(const struct driver *drv, size_t *iter)
{
        if ((!chk_drv(drv)) || (*iter == LLIST_ERR))
                goto stop_condition;
        struct attr *attr = llist_index(&(drv->attrs), *iter);

        if (chk_att(attr)) {
                (*iter)++;
                return attr;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

struct attr *device_prvt_attr_next(const struct device *dev, size_t *iter)
{
        if ((!chk_dev(dev)) || (*iter == LLIST_ERR))
                goto stop_condition;
        struct attr *attr = llist_index(&(dev->prvt_attrs), *iter);

        if (chk_att(attr)) {
                (*iter)++;
                return attr;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

struct attr *device_local_attr_next(const struct device *dev, size_t *iter)
{
        if ((!chk_dev(dev)) || (*iter == LLIST_ERR))
                goto stop_condition;
        struct attr *attr = llist_index(&(dev->local_attrs), *iter);

        if (chk_att(attr)) {
                (*iter)++;
                return attr;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

struct device *device_child_next(const struct device *dev, size_t *iter)
{
        if (!chk_dev(dev) || (*iter == LLIST_ERR))
                goto stop_condition;

        struct devref *chld_ref = llist_index(&(dev->children), *iter);

        if (chk_drf(chld_ref)) {
                (*iter)++;
                return (struct device *)chld_ref->device;
        } else {
                *iter = LLIST_ERR;
                goto stop_condition;
        }

 stop_condition:
        return NULL;
}

inline int lock_read(void)
{
        if (!chk_sdm(sdm) || (sdm->wlock & 1))
                goto call_err;

        sdm->rlock++;
        return 1;

 call_err:
        return 0;
}

inline int lock_write(void)
{
        if (!chk_sdm(sdm) || (sdm->wlock & 1) || sdm->rlock)
                goto call_err;

        sdm->wlock = sdm->wlock | 1;    // only mutate first bit
        return 1;

 call_err:
        return 0;
}

inline int unlock_read(void)
{
        if (!chk_sdm(sdm) || !sdm->rlock)
                goto call_err;

        sdm->rlock--;
        return 1;

 call_err:
        return 0;
}

inline int unlock_write(void)
{
        if (!chk_sdm(sdm))
                goto call_err;

        sdm->wlock = sdm->wlock & ((unsigned short int)(~1));   // only mutate first bit
        return 1;

 call_err:
        return 0;
}

inline int pin_request(struct device *dev, short int pin)
{
        if (!chk_sdm(sdm) || (pin < 0) || (pin > PINNUM))
                goto call_err;

        struct devref *pinref = &sdm->pins[pin];

        if (chk_drf(pinref))
                goto occupied;

        pinref->magic = DRF_MAGIC;
        pinref->device = dev;

        return 1;

 occupied:
 call_err:
        return 0;
}

inline int pin_free(short int pin)
{
        if (!chk_sdm(sdm) || (pin < 0) || (pin > PINNUM))
                goto call_err;

        struct devref *pinref = &sdm->pins[pin];

        if (!chk_drf(pinref))
                goto empty;

        memset(pinref, 0, sizeof(struct devref));

        return 1;

 empty:
 call_err:
        return 0;
}

inline struct device *pin_device(short int pin)
{
        if (!chk_sdm(sdm) || (pin < 0) || (pin > PINNUM))
                goto call_err;

        struct devref *pinref = &sdm->pins[pin];

        if (!chk_drf(pinref))
                goto empty;

        return pinref->device;

 empty:
 call_err:
        return NULL;
}

struct str *push_str(const char *str, size_t size)
{
        if (!chk_sdm(sdm) || !str || (size == 0))
                goto call_err;

        SDM_DEBUG_MSG("push string %s of size %ld", str, size);
        struct str *heap_str = llist_push_blank(&sdm->strings,
                                                sizeof(struct str) +
                                                sizeof(char) * (size - 1));

        if (!heap_str)
                goto alloc_err;
        heap_str->magic = STR_MAGIC;
        heap_str->size = size;
        memcpy(&heap_str->string, str, sizeof(char) * (size));

        return heap_str;

 alloc_err:
 call_err:
        return NULL;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

void __devices_free_nodes(void *storage, size_t storage_sz, const void *data,
                          size_t data_sz)
{
        struct device *dev = storage;

        if (!chk_sdm(sdm) || !chk_dev(dev))
                goto call_err;
        SDM_DEBUG_MSG("clearing device %s", dev->name->string);
        llist_map(&dev->prvt_attrs, __free_attr, NULL, 0);
        llist_map(&dev->local_attrs, __free_attr, NULL, 0);
        llist_free(&dev->prvt_attrs);
        llist_free(&dev->local_attrs);
        llist_free(&dev->children);
        size_t name_ind = llist_search_addr(&sdm->strings, dev->name);

        if (name_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, name_ind);
        } else {
                SDM_DEBUG_MSG("cannot find device name node %s",
                              dev->name->string);
        }

 call_err:
        return;
}

void __drivers_free_nodes(void *storage, size_t storage_sz, const void *data,
                          size_t data_sz)
{
        struct driver *drv = storage;

        if (!chk_drv(drv))
                goto call_err;
        SDM_DEBUG_MSG("clearing driver %s", drv->name->string);
        llist_map(&drv->attrs, __free_attr, NULL, 0);
        llist_map(&drv->methods, __free_method, NULL, 0);
        llist_free(&drv->attrs);
        llist_free(&drv->methods);
        llist_free(&drv->attached);
        size_t name_ind = llist_search_addr(&sdm->strings, drv->name);

        if (name_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, name_ind);
        } else {
                SDM_DEBUG_MSG("cannot find device name node %s",
                              drv->name->string);
        }

 call_err:
        return;
}

void __free_attr(void *storage, size_t storage_sz, const void *data,
                 size_t data_sz)
{
        struct attr *attr = storage;

        if (!chk_att(attr) || !chk_str(attr->name) || !chk_str(attr->desc))
                goto call_err;

        SDM_DEBUG_MSG("clearing attribute %s", attr->name->string);
        size_t name_ind = llist_search_addr(&sdm->strings, attr->name);

        if (name_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, name_ind);
        } else {
                SDM_DEBUG_MSG("cannot find driver name node %s",
                              attr->name->string);
        }
        size_t desc_ind = llist_search_addr(&sdm->strings, attr->desc);

        if (desc_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, desc_ind);
        } else {
                SDM_DEBUG_MSG("cannot find driver description node %s",
                              attr->name->string);
        }
        if (attr->type == string) {
                size_t data_ind =
                    llist_search_addr(&sdm->strings, attr->data.string);
                if (desc_ind == LLIST_ERR) {
                        llist_delete(&sdm->strings, data_ind);
                } else {
                        SDM_DEBUG_MSG("cannot find driver data node %s",
                                      attr->data.string->string);
                }
        }

 call_err:
        return;
}

void __free_method(void *storage, size_t storage_sz, const void *data,
                   size_t data_sz)
{
        struct method *met = storage;

        if (!chk_met(met))
                goto call_err;
        SDM_DEBUG_MSG("%s %s", "clearing method", met->name->string);
        size_t name_ind = llist_search_addr(&sdm->strings, met->name);

        if (name_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, name_ind);
        } else {
                SDM_DEBUG_MSG("cannot find driver name node %s",
                              met->name->string);
        }
        size_t desc_ind = llist_search_addr(&sdm->strings, met->desc);

        if (desc_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, desc_ind);
        } else {
                SDM_DEBUG_MSG("cannot find driver description node %s",
                              met->name->string);
        }

 call_err:
        return;
}

int __drivers_search_name(void *storage, size_t storage_sz, const void *data,
                          size_t data_sz)
{
        const char *name = data;
        struct driver *drv = storage;

        if (chk_drv(drv) && name && (drv->name->size == data_sz) &&
            (memcmp(drv->name->string, name, drv->name->size) == 0))
                return 1;
        else
                return 0;
}

int __methods_search_name(void *storage, size_t storage_sz, const void *data,
                          size_t data_sz)
{
        const char *name = data;
        struct method *met = storage;

        if (chk_met(met) && name && (met->name->size == data_sz) &&
            (memcmp(met->name->string, name, met->name->size) == 0))
                return 1;
        else
                return 0;
}

int __attrs_search_name(void *storage, size_t storage_sz, const void *data,
                        size_t data_sz)
{
        const char *name = data;
        struct attr *attr = storage;

        if (chk_att(attr) && name && (attr->name->size == data_sz) &&
            (memcmp(attr->name->string, name, attr->name->size) == 0))
                return 1;
        else
                return 0;
}

int __refs_search_name(void *storage, size_t storage_sz,
                       const void *data, size_t data_sz)
{
        const char *name = data;
        struct devref *drf = storage;

        if (chk_drf(drf) && chk_dev(drf->device) && name &&
            (drf->device->name->size == data_sz) &&
            (memcmp(drf->device->name->string, name, drf->device->name->size) ==
             0))
                return 1;
        else
                return 0;
}

int __devices_search_name(void *storage, size_t storage_sz, const void *data,
                          size_t data_sz)
{
        const char *name = data;
        struct device *dev = storage;

        if (chk_dev(dev) && name && (dev->name->size == data_sz) &&
            (memcmp(dev->name->string, name, dev->name->size) == 0))
                return 1;
        else
                return 0;
}

#pragma GCC diagnostic pop
