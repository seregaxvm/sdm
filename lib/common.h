
/**
 * @file common.h
 * @author Matsievskiy S.V.
 * @brief Definitions of data structures and common functions.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once
#include <llist.h>

/** \defgroup capi C API
 * @brief C methods of sdm.
 *
 * These are low level api.
 * @{
 * @}
 */

/** \defgroup luaapi Lua API
 * @brief Lua methods of sdm.
 *
 * These are user level api accessible from Lua REPL.
 * These functions provide Lua interface to C sdm library.
 * They are accessed from Lua by their names without 'lua_' prefix.
 * @{
 * @}
 */

/** \ingroup capi
 * @{
 */

#if defined(DESKTOPBUILD)

#include <stdlib.h>
#define sdm_malloc(x) calloc(1, x)
#define sdm_free(x) free(x)
#if defined(DEBUG)
#include <stdio.h>
#define SDM_DEBUG_MSG(format, ...) fprintf(stderr, "%s:%d:%s(): "format"\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#else
#define SDM_DEBUG_MSG(format, ...)
#endif

#else

#include <user_interface.h>
#define sdm_malloc(x) os_zalloc(x)
#define sdm_free(x) os_free(x)
#if defined(DEVELOP_VERSION)
#define SDM_DEBUG_ON
#endif
#if defined(SDM_DEBUG_ON)
#define SDM_DEBUG_MSG(format, ...) os_printf("SDM: "format"\n", ##__VA_ARGS__)
#else
#define SDM_DEBUG_MSG(format, ...)
#endif
#if defined(NODE_ERROR)
#define SDM_ERR_MSG(...) NODE_ERR("SDM: %s\n", ##__VA_ARGS__)
#else
#define SDM_ERR_MSG(...)
#endif

#endif

#define DEVSPLTCHAR "-" /**< Number of chip pins. */

#define PINNUM 9 /**< Number of chip pins. This is used for pin control via pin_request(), pin_free() and pin_device() functions. */

#define STR_MAGIC 0xd5 /**< #str magic value */

#define DRF_MAGIC 0x93 /**< #devref magic value */

#define DEV_MAGIC 0x0f /**< #device magic value */

#define DRV_MAGIC 0xc3 /**< #driver magic value */

#define MET_MAGIC 0xe3 /**< #method magic value */

#define ATT_MAGIC 0xbd /**< #attr magic value */

#define SDM_MAGIC 0xd1 /**< #sdm magic value */

/** @brief string.
 *
 * String container. Provides better security because of \p size field.
 */
struct str {

        unsigned int magic; /**< Struct magic number #STR_MAGIC. */

        size_t size; /**< Size of string. */

        char string[1] __attribute__((aligned(__BIGGEST_ALIGNMENT__)));

                                                                    /**< String. */
};

/** @brief Type of #attr data. */
enum lua_types {

        boolean, /**< @see https://www.lua.org/manual/5.1/manual.html#lua_toboolean */

        numeric, /**< @see https://www.lua.org/manual/5.1/manual.html#lua_tonumber */

        string, /**< @see https://www.lua.org/manual/5.1/manual.html#lua_tolstring */
};

/** @brief Container for #attr data. */
union lua_type {

        int boolean; /**< @see https://www.lua.org/manual/5.1/manual.html#lua_toboolean */

        double numeric; /**< @see https://www.lua.org/manual/5.1/manual.html#lua_tonumber */

        struct str *string; /**< @see https://www.lua.org/manual/5.1/manual.html#lua_tolstring */
};

/** @brief Driver method.
 *
 * Method associated with a driver.
 */
struct method {

        unsigned int magic; /**< Struct magic number #MET_MAGIC. */

        struct str *name; /**< Method name. */

        struct str *desc; /**< Method description. */

        int func; /**< Reference to lua function. @see https://www.lua.org/manual/5.1/manual.html#luaL_ref */
};

/** @brief Driver attribute.
 *
 * Attribute associated with a driver.
 * Each attribute is a boolean/numeric/string with two methods:
 * get and set. They are exposed to a device user.
 */
struct attr {

        unsigned int magic; /**< Struct magic number #ATT_MAGIC. */

        struct str *name; /**< Method name. */

        struct str *desc; /**< Method description. */

        int set; /**< Reference to lua setter function. @see https://www.lua.org/manual/5.1/manual.html#luaL_ref */

        int get; /**< Reference to lua getter function. @see https://www.lua.org/manual/5.1/manual.html#luaL_ref */

        enum lua_types type; /**< Type of the attribute. */

        union lua_type data; /**< Attribute data. */
};

/** @brief Driver handle. */
struct driver {

        unsigned int magic; /**< Struct magic number #DRV_MAGIC. */

        int refcount; /**< Driver reference counter. Counts attached devices. Driver cannot be removed if >0. */

        struct str *name; /**< Unique driver name. */

        struct llist methods; /**< List of driver methods. Each of type #method. */

        struct llist attrs; /**< List of driver attributes. Each of type #attr. */

        struct llist attached; /**< List of devices using this driver. */
};

/** @brief Device handle. */
struct device {

        unsigned int magic; /**< Struct magic number #DEV_MAGIC. */

        int enabled; /**< Device is enabled. */

        int refcount;

                /**< Reference counter. Counts children. Device cannot be removed if >0. */

        struct str *name;  /**< Unique device name. */

        struct device *parent;  /**< Device parent. */

        struct driver *driver; /**< Associated #driver. */

        struct llist local_attrs;

                            /**< Local attributes of the device. Each of type #attr. Persist, when driver is released.*/

        struct llist prvt_attrs; /**< Private attributes of the device, copied from #driver. Each of type #attr. Removed, when driver is released. */

        struct llist children; /**< Children references. */
};

/** @brief Device reference. */
struct devref {

        unsigned int magic; /**< Struct magic number #DRF_MAGIC. */

        struct device *device;

                         /**< #device reference. */
};

/** @brief #sdm tree root structure.
 *
 * SDM root structure. It holds lists of all drivers, devices and busses.
 * Even tough it is called tree, it is actually an array of lists.
 * Hierarchy of deices is implemented via #device::parent and #device::children.
 */
struct sdm {

        unsigned int magic; /**< Struct magic number #SDM_MAGIC. */

        short unsigned int rlock;

                            /**< Read lock. There could be unlimited amount of readers as long as nobody writes data. */

        short unsigned int wlock;

                            /**< Write lock. There could be only one writer. Simultaneous read and write is forbidden. Only first bit is used.*/

        struct device *root; /**< Root bus of the system. It has no parent. */

        struct llist drivers; /**< List of drivers. Each of type #driver. */

        struct llist devices; /**< List of devices. Each of type #device. */

        struct llist strings; /**< List of various strings. Each of type #str. */

        struct devref pins[PINNUM];

                              /**< Chip pin references. Each pin has only one associated device. Devices must request pins and free them at need. */
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"

/** @brief pointer to #sdm tree data structure.
 * Only one device tree should exist in system.
 */
extern struct sdm *sdm;

#pragma GCC diagnostic pop

/** @brief Check #str type.
 *
 * Test if pointer is a valid #str struct.
 * @param[in]	str Pointer to #str.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_str(const struct str *str);

/** @brief Check #devref type.
 *
 * Test if pointer is a valid #devref struct.
 * @param[in]	ref Pointer to #device.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_drf(const struct devref *ref);

/** @brief Check #device type.
 *
 * Test if pointer is a valid #device struct.
 * @param[in]	dev Pointer to #device.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_dev(const struct device *dev);

/** @brief Check #driver type.
 *
 * Test if pointer is a valid #driver struct.
 * @param[in]	drv Pointer to #driver.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_drv(const struct driver *drv);

/** @brief Check #method type.
 *
 * Test if pointer is a valid #method struct.
 * @param[in]	met Pointer to #method.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_met(const struct method *met);

/** @brief Check #attr type.
 *
 * Test if pointer is a valid #attr struct.
 * @param[in]	att Pointer to #attr.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_att(const struct attr *att);

/** @brief Check #sdm type.
 *
 * Test if pointer is a valid #sdm struct.
 * @param[in]	sdm Pointer to #sdm.
 *
 * @retval 1 valid.
 * @retval 0 not valid.
 */
int chk_sdm(const struct sdm *sdm);

/** @brief Find index of char in #string.
 *
 * Find first occurrence of char \p ch and return its index.
 * @param[in]	str #string.
 * @param[in]	ch Char to find.
 *
 * @return Index of char \p ch in string \p str.
 * @retval #LLIST_ERR not found.
 */
size_t chr_index(const struct str *str, char ch);

/** @brief Slice C char string.
 *
 * Copy chars form \p str to \p buffer from \p start to \p end.
 * @param[in]	str Original string.
 * @param[out]	buffer Copy target.
 * @param[in]	start Index of first char to copy.
 * @param[in]	end Index of last char to copy.
 */
void slice_str(const char *str, char *buffer, size_t start, size_t end);

/** @brief Integer to string conversion.
 *
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 * \par Webpage
 * <<http://www.strudel.org.uk/itoa/>>
 *
 * @param[in]	value integer to convert.
 * @param[in]	result pointer to result.
 * @param[in]	base base of integer.
 *
 * @return pointer to /p result.
 */
char *itoa(int value, char *result, int base);

/**@}*/
