
/**
 * @file common.c
 * @author Matsievskiy S.V.
 * @brief Definitions of data structures and common functions.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <common.h>

struct sdm *sdm = NULL;

inline int chk_str(const struct str *str)
{
        return (str != NULL) && (str->magic == STR_MAGIC);
}

inline int chk_drf(const struct devref *ref)
{
        return (ref != NULL) && (ref->magic == DRF_MAGIC);
}

inline int chk_dev(const struct device *dev)
{
        return (dev != NULL) && (dev->magic == DEV_MAGIC);
}

inline int chk_drv(const struct driver *drv)
{
        return (drv != NULL) && (drv->magic == DRV_MAGIC);
}

inline int chk_met(const struct method *met)
{
        return (met != NULL) && (met->magic == MET_MAGIC);
}

inline int chk_att(const struct attr *attr)
{
        return (attr != NULL) && (attr->magic == ATT_MAGIC);
}

inline int chk_sdm(const struct sdm *sdmref)
{
        return (sdmref != NULL) && (sdmref->magic == SDM_MAGIC);
}

inline size_t chr_index(const struct str *str, char ch)
{
        if (!chk_str(str))
                goto call_err;
        for (size_t i = 0; i < str->size; i++) {
                if (str->string[i] == ch)
                        return i;
        }

 call_err:
        return LLIST_ERR;
}

void slice_str(const char *str, char *buffer, size_t start, size_t end)
{
        size_t j = 0;

        for (size_t i = start; i <= end; ++i) {
                buffer[j++] = str[i];
        }
        buffer[j] = 0;
}

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 * \par Webpage
 * <<http://www.strudel.org.uk/itoa/>>
 */
char *itoa(int value, char *result, int base)
{
        // check that the base if valid
        if (base < 2 || base > 36) {
                *result = '\0';
                return result;
        }

        char *ptr = result, *ptr1 = result, tmp_char;
        int tmp_value;

        do {
                tmp_value = value;
                value /= base;
                *ptr++ =
                    "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"
                    [35 + (tmp_value - value * base)];
        } while (value);

        // Apply negative sign
        if (tmp_value < 0)
                *ptr++ = '-';
        *ptr-- = '\0';
        while (ptr1 < ptr) {
                tmp_char = *ptr;
                *ptr-- = *ptr1;
                *ptr1++ = tmp_char;
        }
        return result;
}
