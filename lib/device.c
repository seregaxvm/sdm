
/**
 * @file device.c
 * @author Matsievskiy S.V.
 * @brief Device methods.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <tree.h>
#include <device.h>
#include <driver.h>

struct device *device_add(const char *name,
                          size_t name_sz, struct device *parent)
{
        if (!chk_sdm(sdm) || !name || (name_sz == 0))
                goto call_err;

        if (llist_search(&sdm->devices,
                         __devices_search_name, name, name_sz) != LLIST_ERR)
                goto duplicate;

        if ((!(chk_dev(parent))) || (!parent->enabled)) {
                goto parent_err;
        }

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto name_alloc_err;

        struct device dev = {
                .magic = DEV_MAGIC,
                .refcount = 0,
                .name = heap_name,
                .parent = parent,
        };

        struct device *dev_heap = llist_push(&sdm->devices, &dev, sizeof(dev));

        if (!dev_heap)
                goto dev_alloc_err;

        struct devref dref = {.magic = DRF_MAGIC,.device = dev_heap };

        if (!llist_push(&parent->children, &dref, sizeof(dref)))
                goto ref_alloc_err;

        parent->refcount++;

        return dev_heap;

 ref_alloc_err:
        // new node is allocated but not fully initialized. it has index 0.
        llist_delete(&sdm->devices, 0);
 dev_alloc_err:
        llist_delete(&sdm->strings, 0);
 name_alloc_err:
 parent_err:
 duplicate:
 call_err:
        return NULL;
}

inline size_t device_count(void)
{
        if (!chk_sdm(sdm))
                goto call_err;

        return llist_size(&sdm->devices);
 call_err:
        return LLIST_ERR;
}

inline size_t children_count(const struct device *dev)
{
        if (!chk_dev(dev))
                goto call_err;

        return llist_size(&dev->children);
 call_err:
        return LLIST_ERR;
}

int device_remove(const char *name, size_t size)
{
        if (!chk_sdm(sdm) || !name || (size == 0))
                goto call_err;

        size_t ind = llist_search(&sdm->devices,
                                  __devices_search_name, name, size);

        if (ind == LLIST_ERR)
                goto not_found;
        struct device *dev = llist_index(&sdm->devices, ind);

        if (dev->enabled)
                goto device_enabled;
        llist_map(&dev->local_attrs, __free_attr, NULL, 0);
        llist_map(&dev->prvt_attrs, __free_attr, NULL, 0);
        llist_delete(&sdm->devices, ind);

        return 1;

 device_enabled:
 not_found:
 call_err:
        return 0;
}

int driver_assign(struct device *dev, struct driver *drv)
{
        if (!(chk_dev(dev)) || !(chk_drv(drv)))
                goto call_err;

        if (!llist_push(&drv->attached, &dev, sizeof(dev)))
                goto alloc_err;

        drv->refcount++;
        dev->driver = drv;
        dev->enabled = 1;

        return 1;

 alloc_err:
 call_err:
        return 0;
}

inline struct driver *driver_attached(const struct device *dev)
{
        if (!chk_dev(dev) || !dev->enabled || !chk_drv(dev->driver))
                goto call_err;

        return dev->driver;

 call_err:
        return NULL;
}

int driver_release(struct device *dev)
{
        struct driver *drv = driver_attached(dev);

        if (!chk_drv(drv) || (children_count(dev) > 0) || !chk_str(dev->name))
                goto call_err;

        size_t ei = llist_search(&drv->attached,
                                 __refs_search_name, dev->name->string,
                                 dev->name->size);

        if (!llist_delete(&drv->attached, ei))
                goto not_found;

        llist_map(&dev->prvt_attrs, __free_attr, NULL, 0);
        llist_free(&dev->prvt_attrs);

        drv->refcount--;
        dev->driver = NULL;
        dev->enabled = 0;

        return 1;

 not_found:
 call_err:
        return 0;
}

void *device_handle(const char *name, size_t size)
{
        if (!chk_sdm(sdm) || !name || (size == 0))
                goto call_err;

        size_t i = llist_search(&sdm->devices,
                                __devices_search_name,
                                name, size);

        if (i == LLIST_ERR)
                goto not_found;

        struct device *d = llist_index(&sdm->devices, i);

        if (chk_dev(d))
                return d;

 not_found:
 call_err:
        return NULL;
}

inline struct device *device_parent(const struct device *dev)
{
        if (!chk_dev(dev) || !chk_dev(dev->parent))
                goto call_err;
        return dev->parent;

 call_err:
        return NULL;
}

inline struct device *device_child(const struct device *dev,
                                   const char *name, size_t name_sz)
{
        if (!chk_dev(dev) || !name || (name_sz == 0))
                goto call_err;

        size_t chi =
            llist_search(&dev->children, __refs_search_name, name, name_sz);
        if (chi == LLIST_ERR)
                goto not_found;

        struct devref *chref = llist_index(&dev->children, chi);

        if (!chk_dev(chref->device))
                goto garbage_found;
        return chref->device;

 garbage_found:
 not_found:
 call_err:
        return NULL;
}

int device_rename(struct device *dev, const char *name, size_t name_sz)
{
        if (!chk_dev(dev))
                goto call_err;

        if (llist_search(&sdm->devices, __devices_search_name, name, name_sz) !=
            LLIST_ERR)
                goto exists;

        struct str *str_new = push_str(name, name_sz);

        if (!str_new)
                goto alloc_err;

        size_t stri = llist_search_addr(&sdm->strings, dev->name);

        dev->name = str_new;
        llist_delete(&sdm->strings, stri);

        return 1;

 alloc_err:
 exists:
 call_err:
        return 0;
}

inline const struct str *device_name(const struct device *dev)
{
        if (!chk_dev(dev))
                goto call_err;
        return dev->name;

 call_err:
        return NULL;
}

struct method *method_dev_handle(const struct device *dev,
                                 const char *name, size_t name_sz)
{
        struct driver *drv = driver_attached(dev);

        if (!drv)
                goto call_err;

        return method_drv_handle(drv, name, name_sz);

 call_err:
        return NULL;
}

struct attr *local_attr_handle(struct device *dev,
                               const char *name, size_t name_sz)
{
        if (!chk_dev(dev))
                goto call_err;

        size_t iatt = llist_search(&dev->local_attrs, __attrs_search_name, name,
                                   name_sz);

        if (iatt == LLIST_ERR)
                goto not_found;

        struct attr *a = llist_index(&dev->local_attrs, iatt);

        if (!chk_att(a))
                goto found_garbage;

        return a;

 found_garbage:
 not_found:
 call_err:
        return NULL;
}

struct attr *local_attr_add_bool(struct device *dev,
                                 const char *name,
                                 size_t name_sz,
                                 const char *desc,
                                 size_t desc_sz, int data, int get, int set)
{
        if (!chk_sdm(sdm) || !chk_dev(dev) || !name || (name_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(dev->local_attrs), __attrs_search_name, name,
                         name_sz);

        if (iatt != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct attr attr = {
                .magic = ATT_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .set = set,
                .get = get,
                .type = boolean,
                .data = {.boolean = (data != 0)}
        };
        struct attr *a = llist_push(&(dev->local_attrs), &attr, sizeof(attr));

        if (!chk_att(a))
                goto alloc_err;

        return a;

 alloc_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        return NULL;
}

struct attr *local_attr_add_num(struct device *dev,
                                const char *name,
                                size_t name_sz,
                                const char *desc,
                                size_t desc_sz, double data, int get, int set)
{
        if (!chk_sdm(sdm) || !chk_dev(dev) || !name || (name_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(dev->local_attrs), __attrs_search_name, name,
                         name_sz);

        if (iatt != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct attr attr = {
                .magic = ATT_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .set = set,
                .get = get,
                .type = numeric,
                .data = {.numeric = data}
        };
        struct attr *a = llist_push(&(dev->local_attrs), &attr, sizeof(attr));

        if (!chk_att(a))
                goto alloc_err;

        return a;

 alloc_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        return NULL;
}

struct attr *local_attr_add_str(struct device *dev,
                                const char *name,
                                size_t name_sz,
                                const char *desc,
                                size_t desc_sz,
                                const char *data, size_t data_sz, int get,
                                int set)
{
        if (!chk_sdm(sdm) || !chk_dev(dev) || !name || (name_sz == 0)
            || !data || (data_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(dev->local_attrs), __attrs_search_name, name,
                         name_sz);

        if (iatt != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct str *heap_data = push_str(data, data_sz);

        if (!heap_data)
                goto alloc_data_err;

        struct attr attr = {
                .magic = ATT_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .set = set,
                .get = get,
                .type = string,
                .data = {.string = heap_data}
        };
        struct attr *a = llist_push(&(dev->local_attrs), &attr, sizeof(attr));

        if (!chk_att(a))
                goto alloc_err;

        return a;

 alloc_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop data
 alloc_data_err:
        llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        return NULL;
}

void local_attr_remove(struct device *dev, struct attr *attr)
{
        if (!chk_dev(dev) || !chk_att(attr))
                goto call_err;

        const struct str *name = attr_name(attr);
        size_t iattr =
            llist_search(&dev->local_attrs, __attrs_search_name, name->string,
                         name->size);

        llist_map(&dev->local_attrs, __free_attr, NULL, 0);
        llist_delete(&dev->local_attrs, iattr);

 call_err:
        return;
}

inline size_t attr_local_count(const struct device *dev)
{
        if (!chk_dev(dev))
                goto call_err;
        return llist_size(&(dev->local_attrs));

 call_err:
        return LLIST_ERR;
}

struct attr *prvt_attr_handle(struct device *dev,
                              const char *name, size_t name_sz)
{
        if (!chk_dev(dev))
                goto call_err;

        size_t iatt =
            llist_search(&dev->prvt_attrs, __attrs_search_name, name, name_sz);

        if (iatt == LLIST_ERR)
                goto not_found;

        struct attr *a = llist_index(&dev->prvt_attrs, iatt);

        if (chk_att(a))
                return a;

 not_found:
 call_err:
        return NULL;
}

int attr_copy(struct device *dev, const char *name, size_t name_sz)
{
        if (!chk_dev(dev) || !name || (name_sz == 0))
                goto call_err;

        struct driver *drv = driver_attached(dev);
        struct attr *att = attr_drv_handle(drv, name, name_sz);
        const struct str *att_name = attr_name(att);

        if (!att_name)
                goto call_err;

        SDM_DEBUG_MSG("copying attribute %s to device %s", name,
                      dev->name->string);
        struct str *att_name_cpy = push_str(att_name->string, att_name->size);

        if (!att_name_cpy)
                goto name_alloc_err;
        SDM_DEBUG_MSG("attr %s name copied", name);

        const struct str *att_desc = attr_desc(att);
        struct str *att_desc_cpy = NULL;

        if (att_desc) {
                att_desc_cpy = push_str(att_desc->string, att_desc->size);
                if (!att_desc_cpy)
                        goto desc_alloc_err;
        }
        SDM_DEBUG_MSG("attr %s desc copied", name);

        struct attr *att_cpy =
            llist_push(&dev->prvt_attrs, att, sizeof(struct attr));
        if (!att_cpy)
                goto alloc_err;

        att_cpy->name = att_name_cpy;
        att_cpy->desc = att_desc_cpy;

        // copy attribute data without destroing original
        if (attr_type(att_cpy) == string) {
                union lua_type data = attr_data(att);

                if (!attr_deref_str
                    (att_cpy, data.string->string, data.string->size))
                        goto strcpy_err;
                SDM_DEBUG_MSG("attr %s data copied", name);
        }
        return 1;

 strcpy_err:
        // new node is allocated but not fully initialized. it has index 0.
        llist_delete(&dev->prvt_attrs, 0);      // pop data
 alloc_err:
        if (att_desc)
                llist_delete(&sdm->strings, 0); // pop desc
 desc_alloc_err:
        llist_delete(&sdm->strings, 0); // pop name
 name_alloc_err:
 call_err:
        return 0;
}

void prvt_attr_remove(struct device *dev, struct attr *attr)
{
        if (!chk_dev(dev) || !chk_att(attr))
                goto call_err;

        const struct str *name = attr_name(attr);
        size_t iattr =
            llist_search(&dev->prvt_attrs, __attrs_search_name, name->string,
                         name->size);

        llist_map(&dev->prvt_attrs, __free_attr, NULL, 0);
        llist_delete(&dev->prvt_attrs, iattr);

 call_err:
        return;
}

inline size_t attr_prvt_count(const struct device *dev)
{
        if (!chk_dev(dev))
                goto call_err;
        return llist_size(&(dev->prvt_attrs));

 call_err:
        return LLIST_ERR;
}

struct attr *attr_dev_handle(struct device *dev,
                             const char *name, size_t name_sz)
{
        struct driver *drv = driver_attached(dev);

        if (!drv)
                goto call_err;

        size_t iatt =
            llist_search(&drv->attrs, __attrs_search_name, name, name_sz);

        if (iatt == LLIST_ERR)
                goto not_found;

        struct attr *a = llist_index(&drv->attrs, iatt);

        if (chk_att(a))
                return a;

 not_found:
 call_err:
        return NULL;
}
