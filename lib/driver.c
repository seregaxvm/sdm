
/**
 * @file driver.c
 * @author Matsievskiy S.V.
 * @brief Driver methods.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <tree.h>
#include <driver.h>

struct driver *driver_add(const char *name, size_t size)
{
        if (!chk_sdm(sdm))
                goto call_err;

        size_t idr = llist_search(&sdm->drivers, __drivers_search_name,
                                  name, size);

        if (idr != LLIST_ERR)
                goto call_err;

        struct str *heap_name = push_str(name, size);

        if (!heap_name)
                goto name_alloc_err;

        struct driver proto = {.magic = DRV_MAGIC,
                .refcount = 0,
                .name = heap_name
        };
        struct driver *drv = llist_push(&sdm->drivers, &proto, sizeof(proto));

        if (!chk_drv(drv))
                goto drv_alloc_err;

        SDM_DEBUG_MSG("added driver %s", name);
        return drv;

 drv_alloc_err:
        llist_delete(&sdm->strings, 0);
 name_alloc_err:
 call_err:
        SDM_DEBUG_MSG("driver adding failed");
        return NULL;
}

struct driver *driver_handle(const char *name, size_t size)
{
        if (!chk_sdm(sdm))
                goto call_err;

        size_t idr = llist_search(&sdm->drivers, __drivers_search_name,
                                  name, size);

        if (idr == LLIST_ERR)
                goto call_err;

        struct driver *drv = llist_index(&sdm->drivers, idr);

        if (!chk_drv(drv))
                goto garbage;
        SDM_DEBUG_MSG("found driver handle %s", name);
        return drv;

 garbage:
        SDM_DEBUG_MSG("unexpected type of data");
 call_err:
        SDM_DEBUG_MSG("driver handle not found");
        return NULL;
}

inline size_t driver_index(const struct driver *drv)
{
        if (!chk_sdm(sdm) || !chk_drv(drv))
                goto call_err;
        const struct str *name = driver_name(drv);

        return llist_search(&sdm->drivers, __drivers_search_name,
                            name->string, name->size);

 call_err:
        return LLIST_ERR;
}

inline const struct str *driver_name(const struct driver *drv)
{
        if (!chk_drv(drv))
                goto call_err;
        return drv->name;

 call_err:
        return NULL;
}

void driver_remove(struct driver *drv)
{
        if (!chk_sdm(sdm) || !chk_drv(drv) || (drv->refcount > 0))
                goto call_err;
        const struct str *name = driver_name(drv);
        size_t idr = llist_search(&sdm->drivers, __drivers_search_name,
                                  name->string, name->size);

        llist_free(&(drv->methods));
        llist_map(&(drv->attrs), __free_attr, NULL, 0);
        llist_free(&(drv->attrs));
        llist_delete(&sdm->drivers, idr);

 call_err:
        return;
}

struct method *method_add(struct driver *drv,
                          const char *name,
                          size_t name_sz,
                          const char *desc, size_t desc_sz, int func)
{
        if (!chk_sdm(sdm) || !chk_drv(drv) || !name || (name_sz == 0))
                goto call_err;
        size_t imet = llist_search(&(drv->methods), __methods_search_name, name,
                                   name_sz);

        if (imet != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct method met = {.magic = MET_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .func = func
        };
        struct method *m = llist_push(&(drv->methods), &met, sizeof(met));

        if (!chk_met(m))
                goto alloc_err;
        SDM_DEBUG_MSG("driver method %s added", name);
        return m;

 alloc_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        SDM_DEBUG_MSG("driver method %s add fail", name);
        return NULL;
}

struct method *method_drv_handle(struct driver *drv,
                                 const char *name, size_t name_sz)
{
        if (!chk_drv(drv) || !name || (name_sz == 0))
                goto call_err;
        size_t imet = llist_search(&(drv->methods), __methods_search_name, name,
                                   name_sz);

        if (imet == LLIST_ERR)
                goto call_err;

        struct method *m = llist_index(&(drv->methods), imet);

        if (!chk_met(m))
                goto call_err;

        SDM_DEBUG_MSG("driver method %s handle found", name);
        return m;

 call_err:
        SDM_DEBUG_MSG("driver method %s handle not found", name);
        return NULL;
}

inline size_t driver_count(void)
{
        if (!chk_sdm(sdm))
                goto call_err;
        return llist_size(&sdm->drivers);

 call_err:
        return LLIST_ERR;
}

inline size_t method_count(const struct driver *drv)
{
        if (!chk_drv(drv))
                goto call_err;
        return llist_size(&(drv->methods));

 call_err:
        return LLIST_ERR;
}

inline const struct str *method_name(const struct method *met)
{
        if (!chk_met(met))
                goto call_err;
        return met->name;

 call_err:
        return NULL;
}

inline const struct str *method_desc(const struct method *met)
{
        if (!chk_met(met))
                goto call_err;
        return met->desc;

 call_err:
        return NULL;
}

inline int method_func(const struct method *met)
{
        if (!chk_met(met))
                goto call_err;
        return met->func;

 call_err:
        return -1;
}

int method_remove(struct driver *drv, struct method *met)
{
        if (!chk_sdm(sdm) || !chk_drv(drv) || !chk_met(met))
                goto call_err;

        const struct str *name = method_name(met);
        size_t imet = llist_search(&(drv->methods), __methods_search_name,
                                   name->string, name->size);

        SDM_DEBUG_MSG("%s %s", "clearing method", met->name->string);
        size_t name_ind = llist_search_addr(&sdm->strings, met->name);

        if (name_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, name_ind);
        } else {
                SDM_DEBUG_MSG("cannot find driver name node %s",
                              met->name->string);
        }
        size_t desc_ind = llist_search_addr(&sdm->strings, met->desc);

        if (desc_ind != LLIST_ERR) {
                llist_delete(&sdm->strings, desc_ind);
        } else {
                SDM_DEBUG_MSG("cannot find driver description node %s",
                              met->name->string);
        }

        llist_delete(&(drv->methods), imet);
        return 1;

 call_err:
        return 0;
}

struct attr *attr_add_bool(struct driver *drv,
                           const char *name,
                           size_t name_sz,
                           const char *desc,
                           size_t desc_sz, int data, int get, int set)
{
        if (!chk_sdm(sdm) || !chk_drv(drv) || !name || (name_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(drv->attrs), __attrs_search_name, name, name_sz);

        if (iatt != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct attr attr = {
                .magic = ATT_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .set = set,
                .get = get,
                .type = boolean,
                .data = {.boolean = (data != 0)}
        };
        struct attr *a = llist_push(&(drv->attrs), &attr, sizeof(attr));

        if (!chk_att(a))
                goto alloc_err;

        return a;

 alloc_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        return NULL;
}

struct attr *attr_add_num(struct driver *drv,
                          const char *name,
                          size_t name_sz,
                          const char *desc,
                          size_t desc_sz, double data, int get, int set)
{
        if (!chk_sdm(sdm) || !chk_drv(drv) || !name || (name_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(drv->attrs), __attrs_search_name, name, name_sz);

        if (iatt != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct attr attr = {
                .magic = ATT_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .set = set,
                .get = get,
                .type = numeric,
                .data = {.numeric = data}
        };
        struct attr *a = llist_push(&(drv->attrs), &attr, sizeof(attr));

        if (!chk_att(a))
                goto alloc_err;

        return a;

 alloc_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        return NULL;
}

struct attr *attr_add_str(struct driver *drv,
                          const char *name,
                          size_t name_sz,
                          const char *desc,
                          size_t desc_sz,
                          const char *data, size_t data_sz, int get, int set)
{
        if (!chk_sdm(sdm) || !chk_drv(drv) || !name || (name_sz == 0)
            || !data || (data_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(drv->attrs), __attrs_search_name, name, name_sz);

        if (iatt != LLIST_ERR)
                goto duplicate;

        struct str *heap_name = push_str(name, name_sz);

        if (!heap_name)
                goto alloc_name_err;

        struct str *heap_desc = NULL;

        if (desc && (desc_sz > 0)) {
                heap_desc = push_str(desc, desc_sz);
                if (!heap_desc)
                        goto alloc_desc_err;
        }

        struct str *heap_data = push_str(data, data_sz);

        if (!heap_data)
                goto alloc_data_err;

        struct attr attr = {
                .magic = ATT_MAGIC,
                .name = heap_name,
                .desc = heap_desc,
                .set = set,
                .get = get,
                .type = string,
                .data = {.string = heap_data}
        };
        struct attr *a = llist_push(&(drv->attrs), &attr, sizeof(attr));

        if (!chk_att(a))
                goto alloc_err;

        return a;

 alloc_err:
        llist_delete(&sdm->strings, 0); // pop data
 alloc_data_err:
        if (!desc || (desc_sz == 0))
                llist_delete(&sdm->strings, 0); // pop desc
 alloc_desc_err:
        llist_delete(&sdm->strings, 0); // pop name
 alloc_name_err:
 duplicate:
 call_err:
        return NULL;
}

inline int attr_set_bool(struct attr *attr, int data)
{
        if (!chk_att(attr))
                goto call_err;
        attr->data.boolean = data != 0;
        return 1;

 call_err:
        return 0;
}

inline int attr_set_num(struct attr *attr, double data)
{
        if (!chk_att(attr))
                goto call_err;
        attr->data.numeric = data;
        return 1;

 call_err:
        return 0;
}

int attr_set_str(struct attr *attr, const char *data, size_t size)
{
        if (!chk_sdm(sdm) || !chk_att(attr) || !chk_str(attr->data.string) ||
            !data || (size == 0))
                goto call_err;

        size_t old_ind = llist_search_addr(&sdm->strings, attr->data.string);

        if (old_ind == LLIST_ERR)
                goto call_err;

        struct str *new = push_str(data, size);

        if (!new)
                goto alloc_err;

        // old_int is invalidated by new push
        // assuming old_ind = old_ind + 1
        llist_delete(&sdm->strings, ++old_ind);

        attr->data.string = new;
        return 1;

 alloc_err:
 call_err:
        return 0;
}

int attr_deref_str(struct attr *attr, const char *data, size_t size)
{
        if (!chk_sdm(sdm) || !chk_att(attr) || !data || (size == 0))
                goto call_err;

        struct str *str = push_str(data, size);

        if (!str)
                goto alloc_err;
        attr->data.string = str;
        return 1;

 alloc_err:
 call_err:
        return 0;
}

struct attr *attr_drv_handle(struct driver *drv,
                             const char *name, size_t name_sz)
{
        if (!chk_drv(drv) || !name || (name_sz == 0))
                goto call_err;

        size_t iatt =
            llist_search(&(drv->attrs), __attrs_search_name, name, name_sz);

        if (iatt != LLIST_ERR) {
                struct attr *a = llist_index(&(drv->attrs), iatt);

                if (chk_att(a))
                        return a;
        }

 call_err:
        return NULL;
}

inline enum lua_types attr_type(const struct attr *attr)
{
        return attr->type;
}

inline union lua_type attr_data(const struct attr *attr)
{
        return attr->data;
}

inline const struct str *attr_name(const struct attr *attr)
{
        if (!chk_att(attr))
                goto call_err;
        return attr->name;

 call_err:
        return NULL;
}

inline const struct str *attr_desc(const struct attr *attr)
{
        if (!chk_att(attr))
                goto call_err;
        return attr->desc;

 call_err:
        return NULL;
}

inline int attr_getter(const struct attr *attr)
{
        if (!chk_att(attr))
                goto call_err;
        return attr->get;
 call_err:
        return -1;
}

inline int attr_setter(const struct attr *attr)
{
        if (!chk_att(attr))
                goto call_err;
        return attr->set;
 call_err:
        return -1;
}

void attr_remove(struct driver *drv, struct attr *attr)
{
        if (!chk_drv(drv) || !chk_att(attr))
                goto call_err;

        const struct str *name = attr_name(attr);
        size_t iattr =
            llist_search(&drv->attrs, __attrs_search_name, name->string,
                         name->size);

        llist_map(&drv->attrs, __free_attr, NULL, 0);
        llist_delete(&drv->attrs, iattr);

 call_err:
        return;
}

inline size_t attr_count(const struct driver *drv)
{
        if (!chk_drv(drv))
                goto call_err;
        return llist_size(&(drv->attrs));

 call_err:
        return LLIST_ERR;
}

inline size_t attached_count(const struct driver *drv)
{
        if (!chk_drv(drv))
                goto call_err;
        return llist_size(&(drv->attached));

 call_err:
        return LLIST_ERR;
}
