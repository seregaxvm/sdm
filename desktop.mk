# desktop Makefile
ifneq (,)
This makefile requires GNU Make.
endif

CC := gcc
LUA := lua5.1

DEBUG ?= no
WARNINGS := -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align \
             -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations \
             -Wredundant-decls -Wnested-externs -Winline -Wno-long-long \
             -Wuninitialized -Wconversion -Wstrict-prototypes -Werror

CFLAGS ?= -g -DDESKTOPBUILD -std=gnu11 -fPIC $(WARNINGS)
ifneq ($(DEBUG),no)
CFLAGS += -DDEBUG
endif

VGFLAGS ?= --track-origins=yes --leak-check=full --quiet

LIBNAME := sdm.so
OBJDIR := build
LIBDIR := lib
AUXLIBDIR := auxlib
LUABINDDIR := luabind
DOCDIR := doc
TESTDIR := test

INCLUDES :=	-I $(AUXLIBDIR) \
		-I $(LIBDIR) \
		-I $(LUABINDDIR) \
		 $(shell pkg-config --libs --cflags $(LUA))

SOURCES :=	$(wildcard $(AUXLIBDIR)/*.c) \
		$(wildcard $(LIBDIR)/*.c) \
		$(wildcard $(LUABINDDIR)/*.c)

all: $(LIBNAME)

build_dir:
	@mkdir -p $(OBJDIR)

$(LIBNAME): build_dir $(patsubst %.c,%.o,$(SOURCES))
	$(CC) --shared -fPIC $(CFLAGS) $(wildcard $(OBJDIR)/*.o) -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $(OBJDIR)/$(notdir $@)

distclean: clean
	rm -rf $(OBJDIR)
	rm -rf $(DOCDIR)
	rm -f $(LIBNAME)

clean:
	find . -iname '*~' -delete \
	-o -iname '*.o' -delete \
	-o -iname '*.bak' -delete

test: all
	$(foreach test,$(wildcard $(TESTDIR)/test*.lua),valgrind $(VGFLAGS) $(LUA) $(test);)

indent:
	indent --linux-style --indent-level 8 --braces-after-func-def-line $(shell find . -iname '*.c' -o  -iname '*.h')

doc: Doxyfile
	doxygen $<

.PHONY: doc indent test clean distclean build_dir all
