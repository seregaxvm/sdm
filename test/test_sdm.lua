local sdm = require "sdm"

print("sdm test")

function esp8266_poll(dev)
   return sdm.device_name(dev) == "ESP8266"
end

function esp8266_init(dev)
   assert(sdm.device_name(dev) == "ESP8266")
end

sdm.init()
local root = sdm.device_handle("ESP8266")
assert(root ~= nil)
assert(root == sdm.root())
local drv = sdm.driver_add("ESP8266")
sdm.method_add(drv, "_poll", "poll", esp8266_poll)
sdm.method_add(drv, "_init", "poll", esp8266_init)
assert(sdm.device_poll(root) == true)
assert(sdm.driver_attached(root) ~= nil)

assert(sdm.pin_request(root, 0))
assert(sdm.pin_request(root, 0) == false)
-- assert(sdm.pin_request(root, 15))
-- assert(sdm.pin_request(root, 15) == false)
assert(sdm.pin_request(root, 100) == false)
assert(sdm.pin_device(0) ~= nil)
assert(sdm.pin_device(0) == sdm.root())
-- assert(sdm.pin_device(15) ~= nil)
-- assert(sdm.pin_device(15) == sdm.root())
assert(sdm.pin_device(100) == nil)
assert(sdm.pin_free(0))
assert(sdm.pin_free(0) == false)
assert(sdm.pin_device(0) == nil)
-- assert(sdm.pin_free(15))
-- assert(sdm.pin_free(15) == false)
-- assert(sdm.pin_device(15) == nil)
assert(sdm.pin_free(100) == false)
assert(sdm.pin_device(100) == nil)


sdm.driver_release(root)
assert(sdm.driver_attached(root) == nil)

sdm.destroy()

sdm.init()
local root = sdm.device_handle("ESP8266")
assert(root ~= nil)
assert(root == sdm.root())
sdm.destroy()
