local sdm = require "sdm"

print("driver test")

sdm.init()

local drv = sdm.driver_add("driver")
assert(sdm.handle_type(drv) == "driver")
sdm.driver_add("driver")
sdm.driver_add("third_driver")

local drv2 = sdm.driver_handle("driver")
local phony = sdm.driver_handle("not_driver")
assert(drv ~= nil)
assert(drv == drv2)
assert(phony == nil)

print("driver method test")

local function method(drv, arg)
   return arg
end

sdm.method_add(drv, "met1", "method 1", method)
sdm.method_add(drv, "met3", "method 3", method)
sdm.method_add(drv, "met5", "method 5", method)

local met = sdm.method_drv_handle(drv, "met1")

assert(sdm.handle_type(met) == "method")
assert(sdm.method_drv_handle(drv, "met2") == nil)
assert(sdm.method_drv_handle(phony, "met1") == nil)

local func = sdm.method_func(met)
assert(sdm.method_func(nil) == nil)

assert(func ~= nil)
assert(func(drv, "test") == "test")

assert(sdm.method_desc(sdm.method_drv_handle(drv, "met1")) == "method 1")
met = sdm.method_drv_handle(drv, "met3")
assert(sdm.method_desc(met) == "method 3")
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met5")) == "method 5")
sdm.method_remove(drv, met)
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met1")) == "method 1")
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met3")) == nil)
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met5")) == "method 5")

local mtds = sdm.driver_methods(drv)
assert(mtds.met1 ~= nil)
assert(mtds.met4 == nil)
assert(mtds.met5 ~= nil)
assert(mtds.met1.func ~= nil)
assert(mtds.met5.func ~= nil)
assert(mtds.met1.desc ~= nil)
assert(mtds.met5.desc ~= nil)

print("empty description test")

sdm.method_add(drv, "met100", nil, method)
sdm.method_add(drv, "met200", nil, method)
sdm.method_add(drv, "met300", nil, method)
assert(sdm.method_name(sdm.method_drv_handle(drv, "met100")) == "met100")
assert(sdm.method_name(sdm.method_drv_handle(drv, "met200")) == "met200")
assert(sdm.method_name(sdm.method_drv_handle(drv, "met300")) == "met300")
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met100")) == nil)
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met200")) == nil)
assert(sdm.method_desc(sdm.method_drv_handle(drv, "met300")) == nil)


print("driver attribute test")

local function get(dev, arg)
   return arg
end

local function set(dev, arg)
   return arg
end

assert(sdm.attr_add(drv,
                    "attr1",
                    "attribute 1",
                    true,
                    get,
                    set) ~= nil)
assert(sdm.attr_add(drv,
                    "attr2",
                    "attribute 2",
                    350,
                    get,
                    nil) ~= nil)
assert(sdm.attr_add(drv,
                    "attr3",
                    "attribute 3",
                    "string",
                    nil,
                    nil) ~= nil)

local attr = sdm.attr_drv_handle(drv, "attr1")
assert(attr ~= nil)
assert(sdm.attr_drv_handle(drv, "attr5") == nil)
assert(sdm.attr_drv_handle(phony, "attr1") == nil)

local attrs = sdm.driver_attrs(drv)
assert(attrs.attr1 ~= nil)
assert(attrs.attr2 ~= nil)
assert(attrs.attr3 ~= nil)
assert(attrs.attr4 == nil)
assert(attrs.attr1.set ~= nil)
assert(attrs.attr1.get ~= nil)
assert(attrs.attr1.desc ~= nil)
assert(attrs.attr1.test == nil)
assert(attrs.attr2.set == nil)
assert(attrs.attr2.get ~= nil)
assert(attrs.attr2.desc ~= nil)
assert(attrs.attr2.test == nil)
assert(attrs.attr3.set == nil)
assert(attrs.attr3.get == nil)
assert(attrs.attr3.desc ~= nil)
assert(attrs.attr3.test == nil)


local g, s = sdm.attr_func(attr)
local pg, ps = sdm.attr_func(phony)
assert(g ~= nil)
assert(s ~= nil)
assert(pg == nil)
assert(ps == nil)

assert(g(drv, "get") == "get")
assert(g(drv, "set") == "set")

assert(sdm.attr_data(attr) == true)
sdm.attr_set(attr, false)
assert(sdm.attr_data(attr) == false)

local attr = sdm.attr_drv_handle(drv, "attr2")
assert(attr ~= nil)
assert(sdm.attr_data(attr) == 350)
sdm.attr_set(attr, 121)
assert(sdm.attr_data(attr) == 121)

local attr = sdm.attr_drv_handle(drv, "attr3")
assert(attr ~= nil)
assert(sdm.attr_data(attr) == "string")
sdm.attr_set(attr, "other string")
assert(sdm.attr_data(attr) == "other string")

print("driver free test")

for i=2,15 do
   sdm.driver_add("driver" .. i)
   local drv = sdm.driver_handle("driver" .. i)
   assert(drv ~= nil)
   sdm.method_add(drv, "met1", "method 1", method)
   sdm.method_add(drv, "met3", "method 3", method)
   sdm.method_add(drv, "met5", "method 5", method)
   assert(sdm.method_drv_handle(drv, "met1") ~= nil)
   assert(sdm.method_drv_handle(drv, "met2") == nil)
   assert(sdm.method_drv_handle(drv, "met3") ~= nil)
   assert(sdm.method_drv_handle(drv, "met5") ~= nil)
   sdm.attr_add(drv,
                "attr1",
                "attribute 1",
                true,
                get,
                set)
   sdm.attr_add(drv,
                "attr2",
                "attribute 2",
                350,
                get,
                set)
   sdm.attr_add(drv,
                "attr3",
                "attribute 3",
                "string",
                att1g,
                att1s)
end

sdm.driver_remove("driver3")
sdm.driver_remove("driver8")
sdm.driver_remove("driver12")

sdm.destroy()
