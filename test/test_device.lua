local sdm = require "sdm"

print("device test")

function esp8266_poll(dev)
   return sdm.device_name(dev) == "ESP8266"
end

function esp8266_init(dev)
   assert(sdm.device_name(dev) == "ESP8266")
end

function onewire_poll(dev, parent)
   local name, instance = sdm.device_basename(dev)
   return name == "onewire"
end

function onewire_init(dev)
end

sdm.init()
local root = sdm.device_handle("ESP8266")
assert(sdm.handle_type(root) == "device")
assert(sdm.local_attr_add(root, "id", "id", "123456", nil, nil) ~= nil)
assert(sdm.local_attr_handle(root, "id") ~= nil)
local drv = sdm.driver_add("ESP8266")
sdm.method_add(drv, "_poll", " ", esp8266_poll)
sdm.method_add(drv, "_init", " ", esp8266_init)
drv = sdm.driver_add("onewire")
sdm.method_add(drv, "_poll", " ", onewire_poll)
sdm.method_add(drv, "_init", " ", onewire_init)
local attr = sdm.attr_add(drv,
                          "attr1",
                          "attribute 1",
                          "string",
                          nil,
                          nil)
sdm.attr_add(drv,
             "attr2",
             "attribute 2",
             true,
             function() return "get" end,
             function() return "set" end)
assert(attr ~= nil)

print("device poll test")

sdm.device_poll(root)
assert(sdm.root() ~= nil)

local handle = sdm.device_add("onewire-1", sdm.root())
assert(handle ~= nil)
assert(sdm.device_poll(handle) == true)
assert(sdm.device_parent(handle) == sdm.root())
local phony = sdm.device_add("onewire-1", sdm.root())
assert(phony == nil)
handle = sdm.device_add("onewire-2", sdm.root())
assert(handle ~= nil)
assert(sdm.device_poll(handle) == true)
local attr = sdm.attr_handle(handle, "attr1")
sdm.attr_copy(handle, "attr1")
local attr_cpy = sdm.attr_handle(handle, "attr1")
assert(attr ~= nil)
assert(attr_cpy ~= nil)
assert(attr_cpy ~= attr)
sdm.attr_set(attr_cpy, "string_copy")
assert(sdm.attr_data(attr), "string")
assert(sdm.attr_data(attr_cpy), "string_copy")
local children = sdm.device_children(sdm.root())
assert(sdm.device_name(children["onewire-1"]) == "onewire-1")
assert(sdm.device_name(children["onewire-2"]) == "onewire-2")

sdm.attr_copy(sdm.device_handle("onewire-1"), "attr1")
sdm.attr_copy(sdm.device_handle("onewire-1"), "attr1")
sdm.attr_copy(sdm.device_handle("onewire-1"), "attr2")
sdm.attr_copy(sdm.device_handle("onewire-1"), "attr100")
sdm.driver_release(sdm.device_handle("onewire-1"))

assert(sdm.device_child(sdm.root(), "onewire-1") ~= nil)
assert(sdm.device_name(sdm.device_child(sdm.root(), "onewire-1")) == "onewire-1")

for i,j in pairs(sdm.devices()) do
   assert(sdm.device_name(j) == i)
end

print("device methods test")

local mtds = sdm.device_methods(handle)

assert(mtds._poll ~= nil)
assert(mtds._init ~= nil)
assert(mtds.other == nil)
assert(mtds._poll.func ~= nil)
assert(mtds._init.func ~= nil)
assert(mtds._poll.desc ~= nil)
assert(mtds._init.desc ~= nil)

print("device attributes test")

local attrs = sdm.device_prvt_attrs(handle)

assert(attrs.attr1 ~= nil)
assert(attrs.attr1.desc ~= nil)
assert(attrs.attr2 == nil)

assert(sdm.driver_attached(handle) == drv)
sdm.driver_release(handle)
assert(sdm.driver_attached(handle) == nil)

sdm.driver_release(sdm.root())
assert(sdm.local_attr_handle(sdm.root(), "id") ~= nil)
attrs = sdm.device_local_attrs(sdm.root())
assert(attrs.id ~= nil)
assert(attrs.id.desc ~= nil)
sdm.local_attr_remove(sdm.root(), sdm.local_attr_handle(sdm.root(), "id"))
assert(sdm.local_attr_handle(sdm.root(), "id") == nil)

print("device rename test")

assert(sdm.device_add("test", sdm.root()) ~= nil)
assert(sdm.device_handle("test") ~= nil)
local new_name = sdm.request_name("test")
assert(new_name ~= nil)
assert(sdm.device_rename(sdm.device_handle("test"), new_name) == true)
assert(sdm.device_handle("test") == nil)
assert(sdm.device_handle(new_name) ~= nil)

sdm.destroy()
