
/**
 * @file lua_device.c
 * @author Matsievskiy S.V.
 * @brief Lua device bindings.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <device.h>
#include <driver.h>
#include <tree.h>

#include <lua_driver.h>
#include <lua_device.h>

int lua_device_add(lua_State * L)
{
        if (!lua_isstring(L, 1) || !lua_islightuserdata(L, 2) || !lock_write())
                goto call_err;

        size_t size = 0;
        const char *name = lua_tolstring(L, 1, &size);
        struct device *parent = lua_touserdata(L, 2);
        struct device *dev = device_add(name, size, parent);

        if (!dev)
                goto err;

        lua_pushlightuserdata(L, dev);
        unlock_write();
        return 1;

 err:
        unlock_write();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_handle(lua_State * L)
{
        if (!lua_isstring(L, 1) || !lock_read())
                goto call_err;

        size_t size = 0;
        const char *name = lua_tolstring(L, 1, &size);
        struct device *dev = device_handle(name, size);

        if (!dev)
                goto err;

        lua_pushlightuserdata(L, dev);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_parent(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = device_parent(lua_touserdata(L, 1));

        if (!dev)
                goto err;

        lua_pushlightuserdata(L, dev);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_remove(lua_State * L)
{
        if (!lua_isstring(L, 1) || !lock_write())
                goto call_err;

        size_t size = 0;
        const char *name = lua_tolstring(L, 1, &size);
        int rc = device_remove(name, size);

        lua_pushboolean(L, rc);
        unlock_write();
        return 1;

 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_device_poll(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_write())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        struct device *par = device_parent(dev);

        if (!dev)               // for root device par would be NULL, so no check for it
                goto err;

        size_t drv_cntr = 0;

        while (drv_cntr != LLIST_ERR) {
                struct driver *drv = driver_next(&drv_cntr);

                if (!drv)
                        continue;

                struct method *met = method_drv_handle(drv, "_poll", 5);

                if (!met)
                        continue;

                int m = method_func(met);

                if (m == -1)
                        continue;

                // call function _poll(device_handle, driver_handle, parent_handle)
                lua_rawgeti(L, LUA_REGISTRYINDEX, m);   // 1
                lua_pushlightuserdata(L, dev);  // 2
                lua_pushlightuserdata(L, drv);  // 3
                if (par)
                        lua_pushlightuserdata(L, par);  // 4
                else
                        lua_pushnil(L); // 4
                unlock_write(); // temporarily replace write lock with read lock
                lock_read();
                lua_call(L, 3, 1);
                unlock_read();
                if (!lock_write())
                        goto lock_err;

                if (lua_isboolean(L, -1) &&
                    lua_toboolean(L, -1) && driver_assign(dev, drv)) {
                        // call driver function _init(dev_handle, drv_handle, parent_handle)
                        struct method *meti =
                            method_drv_handle(drv, "_init", 5);
                        const int fi = method_func(meti);

                        // if method present, call function
                        if (meti && (fi != -1)) {
                                lua_rawgeti(L, LUA_REGISTRYINDEX, fi);  // 1
                                lua_pushlightuserdata(L, dev);  // 2
                                lua_pushlightuserdata(L, drv);  // 2
                                if (par)
                                        lua_pushlightuserdata(L, par);  // 4
                                else
                                        lua_pushnil(L); // 4
                                unlock_write(); // temporarily unlock
                                lua_call(L, 3, 1);
                                lock_write();
                        }

                        lua_pushboolean(L, 1);
                        unlock_write();
                        return 1;       // break loop
                }
        }

 err:
        unlock_write();
 lock_err:
 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_device_child(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct device *ch = device_child(dev, name, size);

        if (!ch)
                goto err;

        lua_pushlightuserdata(L, ch);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_rename(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_write())
                goto call_err;

        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct device *dev = lua_touserdata(L, 1);

        if (!device_rename(dev, name, size))
                goto err;

        lua_pushboolean(L, 1);
        unlock_write();
        return 1;

 err:
        unlock_write();
 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_device_name(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        const struct str *str = device_name(dev);

        if (!str)
                goto err;

        lua_pushlstring(L, str->string, str->size);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_basename(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;
        struct device *dev = lua_touserdata(L, 1);
        const struct str *str = device_name(dev);

        if (!str)
                goto err;

        size_t i = chr_index(str, *DEVSPLTCHAR);

        if (i == LLIST_ERR) {
                // string is equal to basename
                lua_pushlstring(L, str->string, str->size);
                lua_pushnil(L);
        } else {
                if (i == 0) {
                        char tmp_str[str->size - 1];

                        slice_str(str->string, tmp_str, 1, str->size - 1);
                        lua_pushnil(L);
                        lua_pushlstring(L, tmp_str, str->size - 1);
                } else if (str->size - 1 == i) {
                        lua_pushlstring(L, str->string, str->size - 1);
                        lua_pushnil(L);
                } else {
                        char tmp_str[str->size - i - 1];

                        slice_str(str->string, tmp_str, i + 1, str->size - 1);
                        lua_pushlstring(L, str->string, i);
                        lua_pushlstring(L, tmp_str, str->size - i - 1);
                }
        }
        unlock_read();
        return 2;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        lua_pushnil(L);
        return 2;
}

int lua_driver_attached(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        struct driver *drv = driver_attached(dev);

        if (!drv)
                goto err;

        lua_pushlightuserdata(L, drv);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_driver_release(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_write())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        struct device *par = device_parent(dev);
        struct driver *drv = driver_attached(dev);
        struct method *met = method_drv_handle(drv, "_free", 5);

        if (met) {
                int m = method_func(met);

                if (m != -1) {
                        // call function _free(device_handle, parent_handle, driver_handle)
                        lua_rawgeti(L, LUA_REGISTRYINDEX, m);   // 1
                        lua_pushlightuserdata(L, dev);  // 2
                        lua_pushlightuserdata(L, drv);  // 3
                        if (par)
                                lua_pushlightuserdata(L, par);  // 4
                        else
                                lua_pushnil(L); // 4
                        unlock_write(); // temporarily unlock
                        lua_call(L, 3, 1);
                        lock_write();
                }
        }

        if (!driver_release(dev))
                goto err;

        lua_pushboolean(L, 1);
        unlock_write();
        return 1;

 err:
        unlock_write();
 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_method_dev_handle(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
                goto call_err;
        struct device *handle = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct method *met = method_dev_handle(handle, name, size);

        if (!met)
                goto err;

        lua_pushlightuserdata(L, met);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_local_attr_add(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_write())
                goto call_err;
        struct device *handle = lua_touserdata(L, 1);
        size_t name_sz = 0;
        size_t desc_sz = 0;
        const char *name = lua_tolstring(L, 2, &name_sz);
        const char *desc = NULL;

        if (lua_isstring(L, 3))
                desc = lua_tolstring(L, 3, &desc_sz);

        int get = -1, set = -1;
        struct attr *attribute = NULL;

        if (lua_isfunction(L, 6))
                set = luaL_ref(L, LUA_REGISTRYINDEX);
        else
                lua_pop(L, 1);

        if (lua_isfunction(L, 5))
                get = luaL_ref(L, LUA_REGISTRYINDEX);
        else
                lua_pop(L, 1);

        if (lua_isboolean(L, 4)) {
                int b = lua_toboolean(L, 4);

                attribute =
                    local_attr_add_bool(handle, name, name_sz, desc, desc_sz,
                                        b, get, set);
        } else if (lua_isnumber(L, 4)) {
                double n = lua_tonumber(L, 4);

                attribute =
                    local_attr_add_num(handle, name, name_sz, desc, desc_sz, n,
                                       get, set);
        } else if (lua_isstring(L, 4)) {
                size_t str_sz = 0;
                const char *str = lua_tolstring(L, 4, &str_sz);

                attribute =
                    local_attr_add_str(handle, name, name_sz, desc, desc_sz,
                                       str, str_sz, get, set);
        } else {
                goto attr_err;
        }

        if (!attribute)
                goto attr_err;

        lua_pushlightuserdata(L, attribute);
        unlock_write();
        return 1;

 attr_err:
        if (get > 0) {
                luaL_unref(L, LUA_REGISTRYINDEX, get);
        }
        if (set > 0) {
                luaL_unref(L, LUA_REGISTRYINDEX, set);
        }
        unlock_write();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_local_attr_handle(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
                goto call_err;
        struct device *handle = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct attr *att = local_attr_handle(handle, name, size);

        if (!att)
                goto err;

        lua_pushlightuserdata(L, att);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_local_attr_remove(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_islightuserdata(L, 2) ||
            !lock_write())
                goto call_err;

        struct device *handle = lua_touserdata(L, 1);
        struct attr *att = lua_touserdata(L, 2);
        int get = attr_getter(att);
        int set = attr_setter(att);

        if (get != -1) {
                luaL_unref(L, LUA_REGISTRYINDEX, get);
        }
        if (set != -1) {
                luaL_unref(L, LUA_REGISTRYINDEX, set);
        }
        local_attr_remove(handle, att);
        lua_pushboolean(L, 1);
        unlock_write();
        return 1;

 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_prvt_attr_handle(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
                goto call_err;
        struct device *handle = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct attr *att = prvt_attr_handle(handle, name, size);

        if (!att)
                goto err;

        lua_pushlightuserdata(L, att);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_attr_copy(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_write())
                goto call_err;
        struct device *handle = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        int rc = attr_copy(handle, name, size);

        lua_pushboolean(L, rc);
        unlock_write();
        return 1;

 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_prvt_attr_remove(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_islightuserdata(L, 2) ||
            !lock_write())
                goto call_err;

        struct device *handle = lua_touserdata(L, 1);
        struct attr *att = lua_touserdata(L, 2);
        int get = attr_getter(att);
        int set = attr_setter(att);

        if (get != -1) {
                luaL_unref(L, LUA_REGISTRYINDEX, get);
        }
        if (set != -1) {
                luaL_unref(L, LUA_REGISTRYINDEX, set);
        }
        prvt_attr_remove(handle, att);
        lua_pushboolean(L, 1);
        unlock_write();
        return 1;

 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_attr_dev_handle(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
                goto call_err;
        struct device *dev = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct attr *att = attr_dev_handle(dev, name, size);

        if (!att)
                goto err;

        lua_pushlightuserdata(L, att);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_attr_handle(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
                goto call_err;
        struct device *dev = lua_touserdata(L, 1);
        size_t size = 0;
        const char *name = lua_tolstring(L, 2, &size);
        struct attr *att = prvt_attr_handle(dev, name, size);

        if (!att) {
                att = attr_dev_handle(dev, name, size);
                if (!att)
                        goto err;
        }

        lua_pushlightuserdata(L, att);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_devices(lua_State * L)
{
        if (!lock_read())
                goto call_err;

        size_t count_d = device_count();

        if ((count_d == LLIST_ERR) || (count_d == 0))
                goto err;

        lua_createtable(L, 0, (int)count_d);    // 1
        size_t dev_cntr = 0;

        while (dev_cntr != LLIST_ERR) {
                struct device *dev = device_next(&dev_cntr);

                if (!dev)
                        continue;

                const struct str *name = device_name(dev);

                lua_pushlstring(L, name->string, name->size);   // 2
                lua_pushlightuserdata(L, dev);                  // 3
                lua_settable(L, -3);                            // 1[2] = 3
        }
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_methods(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        struct driver *drv = driver_attached(dev);
        size_t count_d = method_count(drv);

        if ((count_d == LLIST_ERR) || (count_d == 0))
                goto err;

        lua_createtable(L, 0, (int)count_d);    // 1
        size_t met_cntr = 0;

        while (met_cntr != LLIST_ERR) {
                struct method *met = driver_method_next(drv, &met_cntr);

                if (!met)
                        continue;

                int f = method_func(met);

                if (f == -1)
                        continue;

                const struct str *name = method_name(met);
                const struct str *desc = method_desc(met);

                lua_pushlstring(L, name->string, name->size);   // 2
                lua_createtable(L, 0, 2 + (desc != NULL));      // 3
                if (desc) {
                        lua_pushstring(L, "desc");      // 4
                        lua_pushlstring(L, desc->string, desc->size);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                lua_pushstring(L, "func");      // 4
                lua_rawgeti(L, LUA_REGISTRYINDEX, f);   // 5
                lua_settable(L, -3);    // 3[4] = 5
                lua_pushstring(L, "handle");    // 4
                lua_pushlightuserdata(L, met);  // 5
                lua_settable(L, -3);    // 3[4] = 5
                lua_settable(L, -3);    // 1[2] = 3
        }
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_local_attrs(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);

        size_t count_d = attr_local_count(dev);

        if ((count_d == LLIST_ERR) | (count_d == 0))
                goto err;

        lua_createtable(L, 0, (int)count_d);    // 1
        size_t att_cntr = 0;

        while (att_cntr != LLIST_ERR) {
                struct attr *attr = device_local_attr_next(dev, &att_cntr);

                if (!attr)
                        continue;

                int g = attr_getter(attr);
                int s = attr_setter(attr);

                const struct str *name = attr_name(attr);
                const struct str *desc = attr_desc(attr);

                lua_pushlstring(L, name->string, name->size);   // 2
                lua_createtable(L, 0, 1 + (g >= 0) + (s >= 0) + (desc != NULL));        // 3
                if (desc) {
                        lua_pushstring(L, "desc");      // 4
                        lua_pushlstring(L, desc->string, desc->size);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                if (g >= 0) {
                        lua_pushstring(L, "get");       // 4
                        lua_rawgeti(L, LUA_REGISTRYINDEX, g);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                if (s >= 0) {
                        lua_pushstring(L, "set");       // 4
                        lua_rawgeti(L, LUA_REGISTRYINDEX, s);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                lua_pushstring(L, "handle");    // 4
                lua_pushlightuserdata(L, attr); // 5
                lua_settable(L, -3);    // 3[4] = 5
                lua_settable(L, -3);    // 1[2] = 3
        }
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_prvt_attrs(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);

        size_t count_d = attr_prvt_count(dev);

        if ((count_d == LLIST_ERR) || (count_d == 0))
                goto err;

        lua_createtable(L, 0, (int)count_d);    // 1
        size_t att_cntr = 0;

        while (att_cntr != LLIST_ERR) {
                struct attr *attr = device_prvt_attr_next(dev, &att_cntr);

                if (!attr)
                        continue;

                int g = attr_getter(attr);
                int s = attr_setter(attr);

                const struct str *name = attr_name(attr);
                const struct str *desc = attr_desc(attr);

                lua_pushlstring(L, name->string, name->size);   // 2
                lua_createtable(L, 0, 1 + (g >= 0) + (s >= 0) + (desc != NULL));        // 3
                if (desc) {
                        lua_pushstring(L, "desc");      // 4
                        lua_pushlstring(L, desc->string, desc->size);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                if (g >= 0) {
                        lua_pushstring(L, "get");       // 4
                        lua_rawgeti(L, LUA_REGISTRYINDEX, g);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                if (s >= 0) {
                        lua_pushstring(L, "set");       // 4
                        lua_rawgeti(L, LUA_REGISTRYINDEX, s);   // 5
                        lua_settable(L, -3);    // 3[4] = 5
                }
                lua_pushstring(L, "handle");    // 4
                lua_pushlightuserdata(L, attr); // 5
                lua_settable(L, -3);    // 3[4] = 5
                lua_settable(L, -3);    // 1[2] = 3
        }
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_device_children(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lock_read())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        size_t size = children_count(dev);

        if ((size == LLIST_ERR) || (size == 0))
                goto err;

        lua_createtable(L, 0, (int)size);       // 1

        size_t dev_cntr = 0;
        struct device *chld = device_child_next(dev, &dev_cntr);

        while (dev_cntr != LLIST_ERR) {
                const struct str *name = device_name(chld);

                lua_pushlstring(L, name->string, name->size);
                lua_pushlightuserdata(L, chld);
                lua_settable(L, -3);
                chld = device_child_next(dev, &dev_cntr);
        }

        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;

}
