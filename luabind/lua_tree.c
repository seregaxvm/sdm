
/**
 * @file lua_tree.c
 * @author Matsievskiy S.V.
 * @brief Lua tree bindings.
 * \par Webpage
 * <<https://bitbucket.org/seregaxvm/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <string.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <device.h>
#include <driver.h>
#include <tree.h>

#include <lua_tree.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
int lua_init(lua_State * L)
{
        tree_init();
        return 0;
}

#pragma GCC diagnostic pop

int lua_destroy(lua_State * L)
{
        if (!lock_write())
                goto lock_err;

        size_t drv_cntr = 0;
        struct driver *drv = driver_next(&drv_cntr);

        while (drv_cntr != LLIST_ERR) {
                size_t met_cntr = 0;
                struct method *met = driver_method_next(drv, &met_cntr);

                while (met) {
                        int f = method_func(met);

                        if (f >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, f);
                        met = driver_method_next(drv, &met_cntr);
                }

                size_t att_cntr = 0;
                struct attr *att = driver_attr_next(drv, &att_cntr);

                while (att) {
                        int g = attr_getter(att);

                        if (g >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, g);
                        int s = attr_setter(att);

                        if (s >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, s);
                        att = driver_attr_next(drv, &att_cntr);
                }
                drv = driver_next(&drv_cntr);
        }

        size_t dev_cntr = 0;
        struct device *dev = device_next(&drv_cntr);

        while (dev_cntr != LLIST_ERR) {
                size_t att_cntr = 0;
                struct attr *att = device_local_attr_next(dev, &att_cntr);

                while (att) {
                        int g = attr_getter(att);

                        if (g >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, g);
                        int s = attr_setter(att);

                        if (s >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, s);
                        att = device_local_attr_next(dev, &att_cntr);
                }

                att_cntr = 0;
                att = device_prvt_attr_next(dev, &att_cntr);

                while (att) {
                        int g = attr_getter(att);

                        if (g >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, g);
                        int s = attr_setter(att);

                        if (s >= 0)
                                luaL_unref(L, LUA_REGISTRYINDEX, s);
                        att = device_prvt_attr_next(dev, &att_cntr);
                }
                dev = device_next(&dev_cntr);
        }
        tree_destroy();
        // structure is destroyed so there's nothing to unlock
        lua_pushboolean(L, 1);
        return 1;

 lock_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_driver_prune(lua_State * L)
{
        if (!lock_write())
                goto lock_err;

        size_t drv_cntr = 0;
        struct driver *drv = driver_next(&drv_cntr);

        while (drv_cntr != LLIST_ERR) {
                if (attached_count(drv) == 0) {
                        size_t met_cntr = 0;
                        struct method *met = driver_method_next(drv, &met_cntr);

                        while (met) {
                                int f = method_func(met);

                                if (f >= 0)
                                        luaL_unref(L, LUA_REGISTRYINDEX, f);
                                met = driver_method_next(drv, &met_cntr);
                        }

                        size_t att_cntr = 0;
                        struct attr *att = driver_attr_next(drv, &att_cntr);

                        while (att) {
                                int g = attr_getter(att);

                                if (g >= 0)
                                        luaL_unref(L, LUA_REGISTRYINDEX, g);
                                int s = attr_setter(att);

                                if (s >= 0)
                                        luaL_unref(L, LUA_REGISTRYINDEX, s);
                                att = driver_attr_next(drv, &att_cntr);
                        }
                        driver_remove(drv);
                }
                drv = driver_next(&drv_cntr);
        }

        unlock_write();
        lua_pushboolean(L, 1);
        return 1;

 lock_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_root(lua_State * L)
{
        if (!lock_read())
                goto call_err;

        struct device *dev = sdm_root();

        if (!dev)
                goto err;

        lua_pushlightuserdata(L, dev);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_handle_type(lua_State * L)
{
        if (!lua_islightuserdata(L, 1))
                goto call_err;
        void *handle = lua_touserdata(L, 1);

        if (chk_drv(handle))
                lua_pushstring(L, "driver");
        else if (chk_dev(handle))
                lua_pushstring(L, "device");
        else if (chk_met(handle))
                lua_pushstring(L, "method");
        else if (chk_att(handle))
                lua_pushstring(L, "attribute");
        else
                lua_pushstring(L, "unknown");

        return 1;

 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_pin_request(lua_State * L)
{
        if (!lua_islightuserdata(L, 1) || !lua_isnumber(L, 2) || !lock_write())
                goto call_err;

        struct device *dev = lua_touserdata(L, 1);
        short int pin = (short int)lua_tointeger(L, 2);

        if (!pin_request(dev, pin))
                goto err;

        lua_pushboolean(L, 1);
        unlock_write();
        return 1;

 err:
        unlock_write();
 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_pin_free(lua_State * L)
{
        if (!lua_isnumber(L, 1) || !lock_write())
                goto call_err;

        short int pin = (short int)lua_tointeger(L, 1);

        if (!pin_free(pin))
                goto err;

        lua_pushboolean(L, 1);
        unlock_write();
        return 1;

 err:
        unlock_write();
 call_err:
        lua_pushboolean(L, 0);
        return 1;
}

int lua_pin_device(lua_State * L)
{
        if (!lua_isnumber(L, 1) || !lock_read())
                goto call_err;

        short int pin = (short int)lua_tointeger(L, 1);
        struct device *dev = pin_device(pin);

        if (!dev)
                goto err;

        lua_pushlightuserdata(L, dev);
        unlock_read();
        return 1;

 err:
        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}

int lua_request_name(lua_State * L)
{
        if (!lua_isstring(L, 1) || !lock_read())
                goto call_err;

        size_t size = 0;
        const char *basename = lua_tolstring(L, 1, &size);

        // size of buffer allows up to 6 chars for number part. Number of devices with the same basename probably will never exceed this.
        {
                const size_t numsize = 6;
                size_t bufsize = size + numsize + 2;
                char bufnum[numsize];
                char bufname[bufsize];

                int i = 0;

                while (1) {
                        memset(bufname, '\0', bufsize);
                        strncat(bufname, basename, size);
                        strncat(bufname, DEVSPLTCHAR, size);
                        itoa(i, bufnum, 10);
                        strncat(bufname, bufnum, numsize);
                        if (!device_handle(bufname, strlen(bufname))) {
                                lua_pushstring(L, bufname);
                                unlock_read();
                                return 1;
                        }
                }
        }

        unlock_read();
 call_err:
        lua_pushnil(L);
        return 1;
}
